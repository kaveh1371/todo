<?php

namespace App\Listeners;

use App\Events\UserStoredTodo;
use App\Mail\StoreTodo;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailForStoreTodoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\UserStoredTodo $event
     * @return void
     */
    public function handle(UserStoredTodo $event)
    {
        Mail::to($event->todo->user()->first())
            ->send(new StoreTodo($event->todo));
    }
}
