<?php

namespace App\Http\Controllers\Api;

use App\Events\UserStoredTodo;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteTodoRequest;
use App\Http\Requests\SearchTodoRequest;
use App\Http\Requests\StoreTodoRequest;
use App\Http\Requests\UpdateTodoRequest;
use App\Models\Todo;
use App\Todo\src\Services\TodoService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\SearchTodoRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SearchTodoRequest $request): Response
    {
        $todos = (new TodoService())->search($request->getDTO());

        return new Response($todos, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreTodoRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTodoRequest $request): Response
    {
        $todo = (new TodoService())->store($request->getDTO());
        event(new UserStoredTodo($todo));

        return (new Response([], 201));
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo): Response
    {
        return (new Response($todo->toArray(), 200));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateTodoRequest $request
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTodoRequest $request, Todo $todo): Response
    {
        (new TodoService())->update($request->getDTO());

        return (new Response([], 200));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Requests\DeleteTodoRequest $request
     * @param \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(DeleteTodoRequest $request, Todo $todo): Response
    {
        $todo->delete();

        return (new Response([], 200));
    }
}
