<?php

namespace App\Http\Requests;

use App\Todo\src\DataContracts\SearchTodoDTO;
use Illuminate\Foundation\Http\FormRequest;

class SearchTodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "nullable|exists:users,id",
            "title" => "nullable|string|max:100",
            "status" => "nullable|string|max:20",
            "order_by" => "nullable|array",
            "order_by.created_at" => "in:asc,desc",
            "order_by.id" => "in:asc,desc",
            "page" => "nullable|integer",
            "per_page" => "nullable|integer",
        ];
    }

    /**
     * @return \App\Todo\src\DataContracts\SearchTodoDTO
     */
    public function getDTO()
    {
        $DTO = new SearchTodoDTO();
        $DTO->userId = $this->get("user_id", null);
        $DTO->title = $this->get("title", null);
        $DTO->status = $this->get("status", null);
        $DTO->orderBy = $this->get("order_by", []);
        $DTO->page = $this->get("page", 1);
        $DTO->perPage = $this->get("per_page", 10);

        return $DTO;
    }
}
