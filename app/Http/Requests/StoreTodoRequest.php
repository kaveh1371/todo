<?php

namespace App\Http\Requests;

use App\Todo\src\DataContracts\StoreTodoDTO;
use Illuminate\Foundation\Http\FormRequest;

class StoreTodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title" => "required|string|max:100",
            "status" => "required|string|max:20",
        ];
    }

    public function getDTO()
    {
        $DTO = new StoreTodoDTO();
        $DTO->user = $this->user("api");
        $DTO->title = $this->get("title");
        $DTO->status = $this->get("status");

        return $DTO;
    }
}
