<?php

namespace App\Http\Requests;

use App\Todo\src\DataContracts\UpdateTodoDTO;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route()->parameter("todo")->user_id == $this->user("api")->id;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //"title" => "required|string|max:100",
            //"status" => "required|string|max:20",
        ];
    }

    public function getDTO()
    {
        $DTO = new UpdateTodoDTO();
        $DTO->todo = $this->route()->parameter("todo");
        $DTO->title = $this->get("title");
        $DTO->status = $this->get("status");

        return $DTO;
    }
}
