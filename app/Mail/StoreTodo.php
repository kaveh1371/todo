<?php

namespace App\Mail;

use App\Models\Todo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StoreTodo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Todo $todo
     */
    private $todo;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\Todo $todo
     */
    public function __construct(Todo $todo)
    {
        $this->todo = $todo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.todos.store')->with(["todo" => $this->todo]);
    }
}
