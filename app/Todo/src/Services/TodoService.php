<?php

namespace App\Todo\src\Services;

use App\Models\Todo;
use App\Todo\src\DataContracts\SearchTodoDTO;
use App\Todo\src\DataContracts\StoreTodoDTO;
use App\Todo\src\DataContracts\UpdateTodoDTO;
use Illuminate\Database\Eloquent\Builder;

class TodoService
{
    public function search(SearchTodoDTO $DTO)
    {
        $todos = Todo::query();
        $todos = $this->applyFilters($todos, $DTO);
        $todos = $this->applyOrdering($todos, $DTO);

        return $todos->paginate($DTO->perPage);
    }

    private function applyFilters(Builder $todos, SearchTodoDTO $DTO): Builder
    {
        if ($DTO->userId) {
            $todos = $todos->where("user_id", $DTO->userId);
        }
        if ($DTO->status) {
            $todos = $todos->where("status", $DTO->status);
        }
        if ($DTO->title) {
            $todos = $todos->where("title", "LIKE", "%{$DTO->title}%");
        }

        return $todos;
    }

    private function applyOrdering(Builder $todos, SearchTodoDTO $DTO): Builder
    {
        if (isset($DTO->orderBy["created_at"])) {
            $todos = $todos->orderBy("created_at", $DTO->orderBy["created_at"]);
        }
        if (isset($DTO->orderBy["id"])) {
            $todos = $todos->orderBy("id", $DTO->orderBy["id"]);
        }

        return $todos;
    }

    public function store(StoreTodoDTO $DTO)
    {
        $todo = new Todo();
        $todo->status = $DTO->status;
        $todo->title = $DTO->title;
        $DTO->user->todos()->save($todo);

        return $todo;
    }

    public function update(UpdateTodoDTO $DTO)
    {
        $DTO->todo->update([
            "title" => $DTO->title,
            "status" => $DTO->status,
        ]);
    }
}
