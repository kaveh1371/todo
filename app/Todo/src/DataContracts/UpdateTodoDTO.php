<?php

namespace App\Todo\src\DataContracts;

class UpdateTodoDTO
{
    /**
     * @var \App\Models\Todo $todo
     */
    public $todo;

    public $title;

    public $status;
}
