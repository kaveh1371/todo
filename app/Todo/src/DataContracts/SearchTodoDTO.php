<?php

namespace App\Todo\src\DataContracts;

class SearchTodoDTO
{
    public $userId;

    public $title;

    public $status;

    public $orderBy;

    public $page;

    public $perPage;
}
