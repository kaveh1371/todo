<?php

namespace App\Todo\src\DataContracts;

class StoreTodoDTO
{
    /**
     * @var \App\Models\User $user
     */
    public $user;

    public $title;

    public $status;
}
