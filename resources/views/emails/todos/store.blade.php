<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <div>
        <h1>
            You Created A New Todo
        </h1>
        <p>
            title: {{$todo->title}}
        </p>
        <p>
            status: {{$todo->status}}
        </p>
    </div>
</html>
